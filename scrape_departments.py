# std libs
import asyncio
import json
import os
import sys
import time
from datetime import date
from http import HTTPStatus

# third party libs
import httpx
from bs4 import BeautifulSoup
from rich import print
from rich.console import Console
from rich.progress import (
    Progress,
    SpinnerColumn,
    TimeElapsedColumn
)

# local imports
import utils


BASE_URL = "https://www.py.gov.in"
DATA_DIR = "data"
DEPARTMENT_INDEX_URL = f"{BASE_URL}/department"


async def fetch_department_index(url: str) -> str:
    async with httpx.AsyncClient() as client:
        site_resp = await client.get(url)
    if site_resp.status_code == HTTPStatus.OK:
        return site_resp.text
    return ""


def parse_dept_info(dept_index_html: str) -> list:
    departments = []
    # Note: Upon inspecting the dept page, all department names are inside
    # unordered list. They are in the corresponding hierarchy,
    # ul -> li -> div -> span -> a.
    # No "id" is used, hence we have to rely on the combination of tagname and
    # classname. The page also contains additional ul/li elements which are not
    # department info. Upon closely inspecting the given class names are only
    # used with the department list, hence filtering all div tags which has
    # these class names.
    soup = BeautifulSoup(dept_index_html, features="html.parser")
    all_divs = soup.findAll("div", attrs={"class": "views-field views-field-title"})
    for div in all_divs:
        anchor_tag = div.select("span a")[0]
        departments.append({
            "name": anchor_tag.text,
            "path": anchor_tag.attrs.get("href", ""),
            "url": "",
            "old_urls": []
        })
    return departments


async def fetch_dept_url(department: dict, base_url: str) -> None:
    custom_transport = httpx.AsyncHTTPTransport(retries=3)
    async with httpx.AsyncClient(base_url=base_url, transport=custom_transport) as client:
        if department["path"]:
            try:
                resp = await client.get(department["path"], timeout=80.0)
                if resp.status_code == HTTPStatus.OK:
                    department["url"] = parse_dept_url(resp.text)
            except Exception as ex:
                department["url"] = ""
            finally:
                department.pop("path")


def parse_dept_url(dept_page_html: str) -> str:
    # The page has the match_word text wrapped inside an anchor tag which helps
    # us to fetch the target link directly.
    match_word = "« Goto Department Website »"
    soup = BeautifulSoup(dept_page_html, features="html.parser")
    a_tags = soup.find_all("a", string=match_word, limit=1)
    if a_tags:
        url = a_tags[0].attrs.get("href")
        return url if utils.validate_url(url) else ""
    return ""


async def __main__(base_url: str, dept_index_url: str) -> None:
    with console.status(f"Fetching Department Index: [bold]{dept_index_url}"):
        dept_index_html = await fetch_department_index(dept_index_url)

    if not dept_index_html:
        print("Unable to fetch department directory")
        sys.exit()

    with console.status("Parsing Department Index"):
        departments = parse_dept_info(dept_index_html)
    console.log(f"Departments found: {len(departments)}")

    with progress:
        console.log("Digging deeper to extract departments URL. Please wait.")
        task_id = progress.add_task("progress", total=len(departments))
        # hitting department sites concurrently
        tasks = [fetch_dept_url(dept, base_url) for dept in departments]
        for task in asyncio.as_completed(tasks):
            await task
            progress.update(task_id, advance=1)

    departments.append({
        "name": "Puducherry UT",
        "url": "https://www.py.gov.in",
        "old_urls": ["http://www.pon.nic.in"]
    })

    # Save data to file
    if not os.path.isdir(DATA_DIR):
        os.mkdir(DATA_DIR)
    dest_file = f"{DATA_DIR}/pygov-sites-{date.today()}.json"
    console.log(f"Saving data to: [bold yellow]{dest_file}")
    utils.write_to_file(dest_file, json.dumps({"sites": departments}, indent=2))


if __name__ == "__main__":
    console = Console()
    progress = Progress(
        SpinnerColumn(),
        *Progress.get_default_columns(),
        TimeElapsedColumn()
    )

    start_time = time.time()
    asyncio.run(__main__(BASE_URL, DEPARTMENT_INDEX_URL))
    time_taken = round((time.time() - start_time) / 60, 2)

    console.log(f"Took {time_taken} minutes")
