# std libs
import os
import urllib

# third party libs
from rich.prompt import Confirm, Prompt


def validate_url(url: str) -> bool:
    try:
        result = urllib.parse.urlparse(url)
        return all([result.scheme, result.netloc])
    except:
        return False


def write_to_file(dest_file: str, data: str) -> None:
    if os.path.isfile(dest_file):
        file_name = os.path.basename(dest_file)
        overwrite = Confirm.ask(
            f"File [bold red]{file_name}[/bold red] already exist! Overwrite?")
        if not overwrite:
            file_name = Prompt.ask("Enter new filename: ")
            file_name = os.path.basename(file_name)
            data_dir = os.path.dirname(dest_file)
            dest_file = f"{data_dir}/{file_name}"
            write_to_file(dest_file, data)
    with open(dest_file, "w") as fd:
        fd.write(data)
