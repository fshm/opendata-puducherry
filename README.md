# opendata - Puducherry

opendata - Puducherry is a collection of programs and scripts to scrape, archive & generate data from [Puducherry Goverment websites](https://www.py.gov.in/department).

### Scripts

1. `scrape_departments.py` - generates a `json` file inside [data](data) directory containing name and website of all the departments by scraping https://www.py.gov.in/department

### Setup & Requirements

1. Python >= **"3.10.0"**
2. [Poetry](https://python-poetry.org/docs/)
3. `poetry install --no-root`

### Usage

```bash
python scrape_departments.py
```

### License

1. **Code** - All code in this repository are offered under [GNU GPLv3.0](https://www.gnu.org/licenses/lgpl-3.0.txt)
2. **Data** - All data in this repository are offered under [ODbL 1.0](https://opendatacommons.org/licenses/odbl/1-0/)
